package com.example.mobit.mobitprobedemo;

import java.security.InvalidParameterException;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import android.app.Activity;
import android.content.Intent;
import android.widget.Spinner;
import android.widget.ArrayAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothAdapter;

import com.example.mobit.mobitprobelibrary.MobitProbe;
import com.example.mobit.mobitprobelibrary.MobitBtProbe;
import com.example.mobit.mobitprobelibrary.MobitMopProbe;
import com.example.mobit.mobitprobelibrary.MobitProbeEventListener;

public class MainActivity extends Activity implements MobitProbeEventListener {
    private MobitProbe mMobitProbe;
    private BluetoothDevice mDevice;
    private Thread mConnectDeviceThread;

    public MobitProbe getMobitProbe() {
        return (mMobitProbe);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fillDeviceList();

        findViewById(R.id.spinnerDevices).setEnabled(true);
        findViewById(R.id.buttonStart).setEnabled(true);
        findViewById(R.id.buttonStop).setEnabled(false);
        findViewById(R.id.buttonGetBatteryLifePercent).setEnabled(false);
        findViewById(R.id.buttonReadout).setEnabled(false);
        findViewById(R.id.buttonKohler).setEnabled(false);
    }

    @Override
    public void onBackPressed() {
        try {
            if (mConnectDeviceThread != null) {
                mConnectDeviceThread.interrupt();
                mConnectDeviceThread.join();
            }

            closeDeviceConnection();
        } catch (Throwable t) {
            Toast.makeText(getBaseContext(), t.toString(), Toast.LENGTH_SHORT).show();
        }
        finish();
    }

    public void onStartClicked(View view) {
        findViewById(R.id.spinnerDevices).setEnabled(false);
        findViewById(R.id.buttonStart).setEnabled(false);
        findViewById(R.id.buttonStop).setEnabled(true);

        Spinner spinner = findViewById(R.id.spinnerDevices);
        mDevice = ((DevicesSpinnerObject) spinner.getSelectedItem()).mDevice;
        mConnectDeviceThread = new Thread(new Runnable() {
            @Override
            public void run() {
                connectDeviceLoop(mDevice, 0);
            }
        });
        mConnectDeviceThread.start();
    }

    public void onStopClicked(View view) {
        try {
            if (mConnectDeviceThread != null) {
                mConnectDeviceThread.interrupt();
                mConnectDeviceThread.join();
                mConnectDeviceThread = null;
            }

            closeDeviceConnection();
            findViewById(R.id.spinnerDevices).setEnabled(true);
            findViewById(R.id.buttonStart).setEnabled(true);
            findViewById(R.id.buttonStop).setEnabled(false);
            findViewById(R.id.buttonGetBatteryLifePercent).setEnabled(false);
            findViewById(R.id.buttonReadout).setEnabled(false);
            findViewById(R.id.buttonKohler).setEnabled(false);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    public void onGetBatteryLifePercentClicked(View view) {
        try {
            Toast.makeText(getApplicationContext(), String.format("batteryLifePercent = %d",
                    mMobitProbe.getBatteryLifePercent()), Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    public void onReadoutButtonClicked(View view) {
        Intent intent = new Intent(this, MeterReadingActivity.class);
        MeterReadingActivity.mMainActivity = this;
        MeterReadingActivity.mKohler = false;
        startActivity(intent);
    }

    public void onKohlerButtonClicked(View view) {
        Intent intent = new Intent(this, MeterReadingActivity.class);
        MeterReadingActivity.mMainActivity = this;
        MeterReadingActivity.mKohler = true;
        startActivity(intent);
    }

    public void onConnection() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                findViewById(R.id.buttonGetBatteryLifePercent).setEnabled(true);
                findViewById(R.id.buttonReadout).setEnabled(true);
                findViewById(R.id.buttonKohler).setEnabled(true);
            }
        });
    }

    public void onConnectionReset() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), "onConnectionReset", Toast.LENGTH_SHORT).show();
                findViewById(R.id.buttonGetBatteryLifePercent).setEnabled(false);
                findViewById(R.id.buttonReadout).setEnabled(false);
                findViewById(R.id.buttonKohler).setEnabled(false);
            }
        });

        mConnectDeviceThread = new Thread(new Runnable() {
            @Override
            public void run() {
                connectDeviceLoop(mDevice, 5000);
            }
        });
        mConnectDeviceThread.start();
    }

    public void onPowerStatusChanged() {
        toastOnUiThread("onPowerStatusChanged", Toast.LENGTH_SHORT);
    }

    public void onTriggerButtonPressed(int buttonId) {
        toastOnUiThread("onTriggerButtonPressed", Toast.LENGTH_SHORT);
    }

    private void fillDeviceList() {
        ArrayAdapter<DevicesSpinnerObject> adapter = new ArrayAdapter<DevicesSpinnerObject>(this,
                android.R.layout.simple_spinner_dropdown_item);

        for (BluetoothDevice device : BluetoothAdapter.getDefaultAdapter().getBondedDevices()) {
            if (device.getName().startsWith("MOP-") || device.getName().startsWith("M-") ||
                    device.getName().startsWith("M-OPTPRB")) {
                adapter.add(new DevicesSpinnerObject(device));
            }
        }

        ((Spinner) findViewById(R.id.spinnerDevices)).setAdapter(adapter);
    }

    private void connectDevice(BluetoothDevice device) throws Exception {
        MobitProbe mobitProbe;
        if (device.getName().startsWith("MOP-"))
            mobitProbe = new MobitMopProbe(this);
        else if (device.getName().startsWith("MBT-OPTPRB") || device.getName().startsWith("M-"))
            mobitProbe = new MobitBtProbe(this);
        else
            throw new InvalidParameterException();

        try {
            toastOnUiThread("Trying to connect...", Toast.LENGTH_SHORT);
            mobitProbe.connect(device);
            mMobitProbe = mobitProbe;
        } catch (InterruptedException e) {
            mobitProbe.close();
            throw e;
        }
    }

    private void closeDeviceConnection() {
        if (mMobitProbe != null) {
            try {
                mMobitProbe.close();
            } catch (Exception e) {
            }
            mMobitProbe = null;
        }
    }

    private void connectDeviceLoop(BluetoothDevice device, int delay) {
        try {
            while (true) {
                Thread.sleep(delay);
                try {
                    closeDeviceConnection();
                    connectDevice(device);
                    onConnection();
                    return;
                } catch (InterruptedException ie) {
                    throw ie;
                } catch (Exception e) {
                    toastOnUiThread(e.toString(), Toast.LENGTH_SHORT);
                }
                delay = 10000;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void toastOnUiThread(final CharSequence text, final int duration) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), text,
                        duration).show();
            }
        });
    }

    private class DevicesSpinnerObject {
        BluetoothDevice mDevice;

        DevicesSpinnerObject(BluetoothDevice device) {
            mDevice = device;
        }

        @Override
        public String toString() {
            return (mDevice.getName());
        }
    }
}
