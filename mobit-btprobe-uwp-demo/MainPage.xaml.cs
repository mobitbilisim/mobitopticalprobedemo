﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Devices.Bluetooth.Rfcomm;
using Windows.Devices.Enumeration;
using Windows.Devices.SerialCommunication;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Networking.Sockets;
using Windows.Storage.Streams;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace MobitBtProbeUwpDemo
{
	/// <summary>
	/// An empty page that can be used on its own or navigated to within a Frame.
	/// </summary>
	public sealed partial class MainPage : Page
	{
		MobitBtProbe BtProbe = null;
		delegate void DataLineHandlerDelegate(string dataLine);

		public MainPage()
		{
			this.InitializeComponent();
			UpdateToggleConnectionButton();
		}

		private async void ToggleConnectionButton_Click(object sender, RoutedEventArgs e)
		{
			ErrorText.Text = "";
			TestButton.IsEnabled = false;
			ToggleConnectionButton.IsEnabled = false;

			try
			{
				if (BtProbe == null)
				{
					DeviceInformation deviceInfo = await GetProbeDeviceInformationAsync();
					if (deviceInfo == null)
						throw new Exception("Prob bulunamadı");

					MobitBtProbe btProbe = new MobitBtProbe();
					await btProbe.ConnectAsync(deviceInfo);
					BtProbe = btProbe;
				}
				else
				{
					BtProbe.Dispose();
					BtProbe = null;
				}
			}
			catch (Exception exception)
			{
				ErrorText.Text = exception.Message;
			}
			finally
			{
				UpdateToggleConnectionButton();
				TestButton.IsEnabled = true;
				ToggleConnectionButton.IsEnabled = true;
			}
		}

		private async void TestButton_Click(object sender, RoutedEventArgs e)
		{
			TestButton.IsEnabled = false;
			ToggleConnectionButton.IsEnabled = false;
			try
			{
				ErrorText.Text = "";
				FlagTextBox.Text = "";
				IdentificationTextBox.Text = "";
				ReadoutListBox.Items.Clear();
				await ReadByReadOutAsync(DataLineHandlerProc);
			}
			catch (Exception exception)
			{
				ErrorText.Text = exception.Message;
			}
			finally
			{
				TestButton.IsEnabled = true;
				ToggleConnectionButton.IsEnabled = true;
			}
		}

		private async Task ReadByReadOutAsync(DataLineHandlerDelegate dataLineHandlerProc)
		{
			await BtProbe.CmdIrEnableAsync(true);
			try
			{
				await BtProbe.CmdIrSetBaudRateAsync(MobitBtProbe.IR_BAUDRATE.IRBR300);

				await WriteRequestMessageAsync("");
				Tuple<string, string, int> ident = await ReadIdentificationMessageAsync();

				FlagTextBox.Text = ident.Item1;
				IdentificationTextBox.Text = ident.Item2;

				await WriteOptionSelectMessageAsync(0, ident.Item3, 0);
				await BtProbe.CmdIrSetBaudRateAsync(ConverToIrBaudRate(ident.Item3));
				await ReadDataMessageAsync(dataLineHandlerProc);
			}
			finally
			{
				await BtProbe.CmdIrEnableAsync(false);
			}
		}

		private async Task WriteRequestMessageAsync(string devAddr)
		{
			await BtProbe.WriteAsync(new byte[] { (byte)'/', (byte)'?' });
			await BtProbe.WriteAsync(System.Text.Encoding.ASCII.GetBytes(devAddr));
			await BtProbe.WriteAsync(new byte[] { (byte)'!', (byte)'\r', (byte)'\n' });
		}

		private async Task<Tuple<string, string, int>> ReadIdentificationMessageAsync()
		{
			Tuple<byte[], byte> tuple = await ReadUntil(new byte[] { (byte)'\r', (byte)'\n' });
			byte crOrLf = (await BtProbe.ReadAsync(1))[0];
			if ((tuple.Item2 != '\r' || crOrLf != '\n') && (tuple.Item2 != '\n' || crOrLf != '\r'))
				ThrowMeterCommunicationError();

			if (tuple.Item1.Length < (1 + 3 + 1))
				ThrowMeterCommunicationError();

			if (tuple.Item1[0] != '/')
				ThrowMeterCommunicationError();

			return(new Tuple<string, string, int>(System.Text.Encoding.ASCII.GetString(tuple.Item1, 1, 3),
				System.Text.Encoding.ASCII.GetString(tuple.Item1, 5, tuple.Item1.Length - 5),
				(tuple.Item1[4] - '0')));
		}

		private async Task WriteOptionSelectMessageAsync(int protocol, int baudRateId, int mode)
		{
			await BtProbe.WriteAsync(new byte[] { (byte)0x06, (byte)('0' + protocol),
				(byte)('0' + baudRateId), (byte)('0' + mode), (byte)'\r', (byte)'\n' });
		}

		private async Task ReadDataMessageAsync(DataLineHandlerDelegate dataLineHandlerProc)
		{
			if ((await BtProbe.ReadAsync(1))[0] != 0x02)
				ThrowMeterCommunicationError();

			byte bcc = 0;
			while (true)
			{
				Tuple<byte[], byte> tuple = await ReadUntil(new byte[] { (byte)'\r', (byte)'\n', 0x03 });
				bcc = CalculateBCC(bcc, tuple.Item1);
				bcc = CalculateBCC(bcc, tuple.Item2);
				if (tuple.Item2 == 0x03)
					break;

				byte crOrLf = (await BtProbe.ReadAsync(1))[0];
				bcc = CalculateBCC(bcc, crOrLf);

				if ((tuple.Item2 != '\r' || crOrLf != '\n') && (tuple.Item2 != '\n' || crOrLf != '\r'))
					ThrowMeterCommunicationError();

				dataLineHandlerProc(System.Text.Encoding.ASCII.GetString(tuple.Item1, 
					0, tuple.Item1.Length));
			}

			if ((await BtProbe.ReadAsync(1))[0] != bcc)
				ThrowMeterCommunicationError();
		}

		private async Task<DeviceInformation> GetProbeDeviceInformationAsync()
		{
			DeviceInformationCollection deviceInfoColl = await DeviceInformation.FindAllAsync(SerialDevice.GetDeviceSelector());
			foreach(DeviceInformation deviceInfo in deviceInfoColl)
			{
				if (deviceInfo.Name == "MBT-OPTPRB")
					return (deviceInfo);
			}
			return (null);
		}

		private async Task<Tuple<byte[], byte>> ReadUntil(byte[] stopChars)
		{
			List<byte> byteList = new List<byte>();
			while (true)
			{
				byte data = (await BtProbe.ReadAsync(1))[0];
				foreach (byte stopChar in stopChars)
				{
					if (data == stopChar)
						return(new Tuple<byte[], byte>(byteList.ToArray(), stopChar));
				}
				byteList.Add(data);
			}
		}

		private MobitBtProbe.IR_BAUDRATE ConverToIrBaudRate(int baudRateId)
		{
			switch (baudRateId)
			{
				case 0: return (MobitBtProbe.IR_BAUDRATE.IRBR300);
				case 1: return (MobitBtProbe.IR_BAUDRATE.IRBR600);
				case 2: return (MobitBtProbe.IR_BAUDRATE.IRBR1200);
				case 3: return (MobitBtProbe.IR_BAUDRATE.IRBR2400);
				case 4: return (MobitBtProbe.IR_BAUDRATE.IRBR4800);
				case 5: return (MobitBtProbe.IR_BAUDRATE.IRBR9600);
				case 6: return (MobitBtProbe.IR_BAUDRATE.IRBR19200);
				default: throw new Exception("Invalid baudrate");
			}
		}

		private byte CalculateBCC(byte prevBcc, byte data)
		{
			return ((byte)(prevBcc ^ data));
		}

		private byte CalculateBCC(byte prevBcc, byte[] bytes)
		{
			byte bcc = prevBcc;
			foreach (byte data in bytes)
				bcc = CalculateBCC(bcc, data);
			return (bcc);
		}

		private void UpdateToggleConnectionButton()
		{
			ToggleConnectionButton.Content = (BtProbe == null) ? "Bağlan" : "Kes";
		}

		private void DataLineHandlerProc(string dataLine)
		{
			ReadoutListBox.Items.Add(dataLine);
		}

		private void ThrowMeterCommunicationError()
		{
			throw new Exception("Meter Communication Error");
		}
	}
}
