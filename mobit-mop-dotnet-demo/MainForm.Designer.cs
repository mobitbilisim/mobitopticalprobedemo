﻿namespace mobit_mop_dotnet_demo
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.MainMenu mainMenu1;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.mainMenu1 = new System.Windows.Forms.MainMenu();
			this.deviceComboBox = new System.Windows.Forms.ComboBox();
			this.label1 = new System.Windows.Forms.Label();
			this.buttonConnect = new System.Windows.Forms.Button();
			this.buttonDisconnect = new System.Windows.Forms.Button();
			this.buttonLifePercent = new System.Windows.Forms.Button();
			this.buttonReadout = new System.Windows.Forms.Button();
			this.buttonKohler = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// deviceComboBox
			// 
			this.deviceComboBox.Location = new System.Drawing.Point(3, 27);
			this.deviceComboBox.Name = "deviceComboBox";
			this.deviceComboBox.Size = new System.Drawing.Size(234, 22);
			this.deviceComboBox.TabIndex = 0;
			// 
			// label1
			// 
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.label1.BackColor = System.Drawing.Color.Blue;
			this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
			this.label1.ForeColor = System.Drawing.SystemColors.Window;
			this.label1.Location = new System.Drawing.Point(3, 4);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(234, 20);
			this.label1.Text = "Device";
			this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// buttonConnect
			// 
			this.buttonConnect.Location = new System.Drawing.Point(3, 56);
			this.buttonConnect.Name = "buttonConnect";
			this.buttonConnect.Size = new System.Drawing.Size(234, 35);
			this.buttonConnect.TabIndex = 2;
			this.buttonConnect.Text = "Connect";
			this.buttonConnect.Click += new System.EventHandler(this.buttonConnect_Click);
			// 
			// buttonDisconnect
			// 
			this.buttonDisconnect.Location = new System.Drawing.Point(3, 98);
			this.buttonDisconnect.Name = "buttonDisconnect";
			this.buttonDisconnect.Size = new System.Drawing.Size(234, 35);
			this.buttonDisconnect.TabIndex = 3;
			this.buttonDisconnect.Text = "Disconnect";
			this.buttonDisconnect.Click += new System.EventHandler(this.buttonDisconnect_Click);
			// 
			// buttonLifePercent
			// 
			this.buttonLifePercent.Location = new System.Drawing.Point(3, 140);
			this.buttonLifePercent.Name = "buttonLifePercent";
			this.buttonLifePercent.Size = new System.Drawing.Size(234, 35);
			this.buttonLifePercent.TabIndex = 4;
			this.buttonLifePercent.Text = "Get battery life percent";
			this.buttonLifePercent.Click += new System.EventHandler(this.buttonLifePercent_Click);
			// 
			// buttonReadout
			// 
			this.buttonReadout.Location = new System.Drawing.Point(3, 182);
			this.buttonReadout.Name = "buttonReadout";
			this.buttonReadout.Size = new System.Drawing.Size(234, 35);
			this.buttonReadout.TabIndex = 5;
			this.buttonReadout.Text = "Readout";
			this.buttonReadout.Click += new System.EventHandler(this.buttonReadout_Click);
			// 
			// buttonKohler
			// 
			this.buttonKohler.Location = new System.Drawing.Point(3, 224);
			this.buttonKohler.Name = "buttonKohler";
			this.buttonKohler.Size = new System.Drawing.Size(234, 35);
			this.buttonKohler.TabIndex = 6;
			this.buttonKohler.Text = "Köhler";
			this.buttonKohler.Click += new System.EventHandler(this.buttonKohler_Click);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
			this.AutoScroll = true;
			this.ClientSize = new System.Drawing.Size(240, 268);
			this.Controls.Add(this.buttonKohler);
			this.Controls.Add(this.buttonReadout);
			this.Controls.Add(this.buttonLifePercent);
			this.Controls.Add(this.buttonDisconnect);
			this.Controls.Add(this.buttonConnect);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.deviceComboBox);
			this.Menu = this.mainMenu1;
			this.MinimizeBox = false;
			this.Name = "MainForm";
			this.Text = "MOP-Demo";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.Closing += new System.ComponentModel.CancelEventHandler(this.MainForm_Closing);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.ComboBox deviceComboBox;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button buttonConnect;
		private System.Windows.Forms.Button buttonDisconnect;
		private System.Windows.Forms.Button buttonLifePercent;
		private System.Windows.Forms.Button buttonReadout;
		private System.Windows.Forms.Button buttonKohler;

	}
}

