﻿using System;
using System.IO;
using System.Threading;
using System.Net.Sockets;
using System.Windows.Forms;
using System.Collections.Generic;

namespace mobit_mop_dotnet_demo
{
	public partial class MeterReadingForm : Form
	{
		bool m_KohlerMode;
		NetworkStream m_Stream;

		public MeterReadingForm(NetworkStream stream, bool kohlerMode)
		{
			m_Stream = stream;
			m_KohlerMode = kohlerMode;
			InitializeComponent();
		}

		private void buttonRead_Click(object sender, EventArgs e)
		{
			try
			{
				List<String> obisList;

				obisListBox.Items.Clear();
				obisListBox.Update();

				if (m_KohlerMode)
					obisList = ReadKohler();
				else
					obisList = ReadOut();

				foreach (string text in obisList)
					obisListBox.Items.Add(text);
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		private List<String> ReadOut()
		{
			IEC62056 iec = new IEC62056(m_Stream);
			iec.WriteRequest(null);
			IEC62056.Identification ident = iec.ReadIdentification();

			iec.WriteOptionSelect(0, ident.baudId, 0);
			return (iec.ReadDataMessageList());
		}

		private List<String> ReadKohler()
		{
			IEC62056 iec = new IEC62056(m_Stream);
			iec.WriteRequest(null);
			IEC62056.Identification ident = iec.ReadIdentification();

			try
			{
				iec.WriteOptionSelect(0, ident.baudId, 1);
				iec.ReadAcknowledgement();

				List<String> obisList = new List<string>();
				obisList.Add(iec.QueryDataBlock('2', "0.0.0()"));
				obisList.Add(iec.QueryDataBlock('2', "1.8.0()"));
				obisList.Add(iec.QueryDataBlock('2', "1.8.1()"));
				obisList.Add(iec.QueryDataBlock('2', "1.8.2()"));
				obisList.Add(iec.QueryDataBlock('2', "1.8.3()"));
				return (obisList);
			}
			finally
			{
				for (int i = 0; i < 5; i++)
				{
					iec.WriteBreak();
					Thread.Sleep(100);
				}
			}
		}
	}
}