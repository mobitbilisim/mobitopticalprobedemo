﻿using System;
using System.IO;
using System.Text;
using System.Threading;
using System.Net.Sockets;
using System.Collections.Generic;

namespace mobit_mop_dotnet_demo
{
	class IEC62056
	{
		public class Identification
		{
			public String devId;
			public String mnfId;
			public int baudId;
		}

		const int SOH = 0x01;
		const int STX = 0x02;
		const int ETX = 0x03;
		const int EOT = 0x04;
		const int ACK = 0x06;
		const int NAK = 0x15;
		const int IR_DATA_TIMEOUT = 3000;

		NetworkStream m_Stream;

		public IEC62056(NetworkStream stream)
		{
			m_Stream = stream;
			ResetInput();
		}

		public void WriteRequest(String devAddr)
		{
			WriteBytes(new byte[] { (byte)'/', (byte)'?' });
			if (devAddr != null)
				WriteBytes(Encoding.ASCII.GetBytes(devAddr));
			WriteBytes(new byte[] { (byte)'!', (byte)'\r', (byte)'\n' });
		}

		public Identification ReadIdentification()
		{
			byte[] data;
			int chr1, chr2;

			chr1 = ReadBytesUntil(out data, new int[] { '\r', '\n' });
			chr2 = ReadByte();
			if ((chr1 != '\r' || chr2 != '\n') && (chr1 != '\n' || chr2 != '\r'))
				throw new InvalidDataException();

			if (data.Length < (1 + 3 + 1))
				throw new InvalidDataException();

			if (data[0] != '/')
				throw new InvalidDataException();

			Identification ident = new Identification();
			ident.devId = Encoding.ASCII.GetString(data, 5, data.Length - 5);
			ident.mnfId = Encoding.ASCII.GetString(data, 1, 3);
			ident.baudId = data[4] - '0';
			return(ident);
		}

		public void WriteOptionSelect(int protocol, int baudId, int mode)
		{
			MemoryStream stream = new MemoryStream();
			stream.WriteByte(ACK);
			stream.WriteByte((byte)('0' + protocol));
			stream.WriteByte((byte)('0' + baudId));
			stream.WriteByte((byte)('0' + mode));
			stream.WriteByte((byte)'\r');
			stream.WriteByte((byte)'\n');
			WriteBytes(stream.ToArray());
		}

		public void ReadAcknowledgement()
		{
			if (ReadByte() != ACK)
				throw new InvalidDataException();
		}

		public void WriteProgrammingCommand(int cmi, int cti, byte[] dataSet)
		{
			MemoryStream stream = new MemoryStream();
			stream.WriteByte(SOH);
			stream.WriteByte((byte)cmi);
			stream.WriteByte((byte)cti);
			if (dataSet != null)
			{
				stream.WriteByte(STX);
				stream.Write(dataSet, 0, dataSet.Length);
			}
			stream.WriteByte(ETX);
			stream.WriteByte(CalculateBcc((byte)0, stream.ToArray(), 1, (int)stream.Length - 1));
			WriteBytes(stream.ToArray());
		}

		public void WriteBreak()
		{
			WriteProgrammingCommand('B', '0', null);
		}

		public byte[] ReadDataMessage()
		{
			byte[] data;

			if (ReadByte() != STX)
				throw new InvalidDataException();

			ReadBytesUntil(out data, new int[] { ETX });

			int bcc = ReadByte();
			/* You shoud check the BCC here */

			return (data);
		}

		public String QueryDataBlock(int cti, String address)
		{
			byte[] data = QueryDataBlock(cti, Encoding.ASCII.GetBytes(address));
			return (System.Text.Encoding.ASCII.GetString(data, 0, data.Length));
		}

		public byte[] QueryDataBlock(int cti, byte[] address)
		{
			WriteProgrammingCommand('R', cti, address);
			return (ReadDataMessage());
		}

		public List<String> ReadDataMessageList()
		{
			if (ReadByte() != STX)
				throw new InvalidDataException();

			List<String> dataList = new List<String>();
			while (true)
			{
				byte[] data;

				int chr1 = ReadBytesUntil(out data, new int[] { '\r', '\n', ETX });
				if (chr1 == ETX)
					break;

				int chr2 = ReadByte();
				if ((chr1 != '\r' || chr2 != '\n') && (chr1 != '\n' || chr2 != '\r'))
					throw new InvalidDataException();

				dataList.Add(Encoding.ASCII.GetString(data, 0, data.Length));
			}

			int bcc = ReadByte();
			/* You shoud check the BCC here */

			return (dataList);
		}

		void WriteBytes(byte[] data)
		{
			m_Stream.Write(data, 0, data.Length);
		}

		public void ResetInput()
		{
			long tickCount = Environment.TickCount;
			while ((Environment.TickCount - tickCount) < 500)
			{
				if (m_Stream.DataAvailable)
				{
					m_Stream.ReadByte();
					tickCount = Environment.TickCount;
				}
				else
				{
					Thread.Sleep(10);
				}
			}
		}

		int ReadByte()
		{
			long tickCount = Environment.TickCount;
			while (!m_Stream.DataAvailable)
			{
				if ((Environment.TickCount - tickCount) > IR_DATA_TIMEOUT)
					throw new TimeoutException();

				Thread.Sleep(10);
			}
			return (m_Stream.ReadByte());
		}

		int ReadBytesUntil(out byte[] data, int[] terminators)
		{
			List<byte> list = new List<byte>();
			while (true)
			{
				int value = ReadByte();
				if (Array.IndexOf(terminators, (byte)value) >= 0)
				{
					data = list.ToArray();
					return ((byte)value);
				}
				list.Add((byte)value);
			}
		}

		byte CalculateBcc(byte prevBcc, byte data)
		{
			return ((byte)(prevBcc ^ data));
		}

		byte CalculateBcc(byte bcc, byte[] data, int start, int length)
		{
			for (int index = 0; index < length; index++)
				bcc = CalculateBcc(bcc, data[start + index]);
			return (bcc);
		}
	}
}
